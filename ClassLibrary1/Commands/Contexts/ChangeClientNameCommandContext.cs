﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Commands.CommandContext;

namespace Domain.Commands.Contexts
{
    public class ChangeClientNameCommandContext : ICommandContext
    {
        public int Id;
        public string Name;
    }
}
