﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Commands.CommandContext;

namespace Domain.Commands.Contexts
{
    public class CreateBillCommandContext : ICommandContext
    {
        public int ClientId;
        public decimal Sum;
    }
}
