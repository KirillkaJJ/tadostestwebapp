﻿using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Commands
{
    public class CreateClientCommand : ICommand<CreateClientContext>
    {
        private readonly IClientService _service;

        public CreateClientCommand(IClientService service)
        {
            _service = service;
        }

        public void Execute(CreateClientContext commandContext)
        {
            _service.Add(commandContext.Name, commandContext.Inn);
        }
    }
}