﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Commands.CommandContext;
using Domain.Queries.Criteria;

namespace Domain.Entities
{
    public class Client : IEntity, ICommandContext, ICriterion
    {
        public int Id { get; protected set; }
        public string Name { get; protected set; }
        public string InnNum { get; protected set; }

        public Client(string name, string innNum)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrWhiteSpace(innNum))
                throw new ArgumentNullException(nameof(innNum));
            //Id = id;
            Name = name;
            InnNum = innNum;
        }

        public Client() { }

        public void ChangeName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
        }
    }
}
