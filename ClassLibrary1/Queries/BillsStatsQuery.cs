﻿using System;
using System.Linq;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Queries.ResponseEnteties;
using Domain.Repositories;

namespace Domain.Queries
{
    public class BillsStatsQuery
                : IQuery<BillsStatsCriterion, BillsStatsResponse>
    {
        private readonly IRepository<Bill> _repository;

        public BillsStatsQuery(IRepository<Bill> repository)
        {
            _repository = repository;
        }

        public BillsStatsResponse Ask(BillsStatsCriterion criterion)
        {
            int totalCount = _repository.All().Count();
            int payedCount = _repository.All().Count(v => v.Paid);
            int unpayedCount = totalCount - payedCount;

            decimal totalSum = _repository.All()
                .Sum(v => v.Summ);

            decimal totalPayedSum = _repository.All()
                .Sum(v => v.Summ);

            decimal totalUnpayedSum = totalSum - totalPayedSum;

            return new BillsStatsResponse
            {
                TotalCount = totalCount,
                PayedCount = payedCount,
                UnpayedCount = unpayedCount,
                TotalSum = totalSum,
                PayedSum = totalPayedSum,
                UnpayedSum = totalUnpayedSum
            };
        }
    }
}