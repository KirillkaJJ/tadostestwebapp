﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Repositories;

namespace Domain.Queries
{
    public class GetClientBillsQuery : IQuery<ClientBillsCriterion, IEnumerable<Bill>>
    {
        private readonly IRepository<Bill> _repository;

        public GetClientBillsQuery(IRepository<Bill> repository)
        {
            _repository = repository;
        }

        public IEnumerable<Bill> Ask(ClientBillsCriterion criterion)
        {
            int offset = criterion.Offset > 0 ? criterion.Offset : 0;

            int count = criterion.Count;
            if (count < 0) return Enumerable.Empty<Bill>();
            else if (count > 100) count = 100;

            return  _repository.All()
                .Where(v => v.ClientId == criterion.ClientId)
                .OrderBy(v => v.CreatedAt.Year)
                .ThenBy(v => v.CreatedAt.Month)
                .ThenBy(v => v.Number)
                .Skip(offset)
                .Take(count)/*
                .Select(v => new             // как по умному это форматировать и где?
                {
                    v.Id,
                    v.Number,
                    CreatedAt = v.CreatedAtAsString,
                    PayedAt = v.PayedAtAsString,
                    Sum = v.Summ,
                    v.DisplayNumber,
                    WasPayed = v.Paid
                })*/;
        }
    }
}