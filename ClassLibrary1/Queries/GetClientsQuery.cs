﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Repositories;

namespace Domain.Queries
{
    public class GetClientsQuery : IQuery<OffsetCountCriterion, IEnumerable<Client>>
    {
        private readonly IRepository<Client> _repository;

        public GetClientsQuery(IRepository<Client> repository)
        {
            _repository = repository;
        }

        public IEnumerable<Client> Ask(OffsetCountCriterion criterion)
        {
            int offset = criterion.Offset > 0 ? criterion.Offset : 0;

            int count = criterion.Count;
            if (count < 0) return Enumerable.Empty<Client>();
            else if (count > 100) count = 100;

            return _repository.All()
                .OrderBy(v => v.Name)
                .Skip(offset)
                .Take(count);
        }
    }
}