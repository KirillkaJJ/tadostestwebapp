﻿namespace Domain.Queries.ResponseEnteties
{
    public class BillsStatsResponse
    {
        public int TotalCount;
        public int PayedCount;
        public int UnpayedCount;
        public decimal TotalSum;
        public decimal PayedSum;
        public decimal UnpayedSum;
    }
}