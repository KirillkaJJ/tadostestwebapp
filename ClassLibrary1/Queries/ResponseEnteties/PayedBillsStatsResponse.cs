﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Queries.ResponseEnteties
{
    public class PayedBillsStatsResponse
    {
        public Client Client;
        public decimal Sum;
    }
}
