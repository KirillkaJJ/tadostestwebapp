﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Domain.Entities;
using Domain.Model;

namespace Domain.Repositories
{
    public class DbRepository<TEntity> : IRepository<TEntity>, IDisposable
        where TEntity : class, IEntity
    {
        private readonly ClientContext _dbContext;

        public DbRepository(ClientContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            Save();
        }

        public IEnumerable<TEntity> All() //todo или IQueriable
        {
            var t = _dbContext.Set<TEntity>().ToImmutableList();
            //var y = t.AsEnumerable();
            return _dbContext.Set<TEntity>().AsEnumerable();
        }

        public void Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            Save();
        }

        public void Delete(int id)
        {
            TEntity entity = _dbContext.Set<TEntity>().Find(id);
            if (entity != null)
            {
                _dbContext.Set<TEntity>().Remove(entity);
                Save();
            }
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}