﻿using Domain.Entities;

namespace Domain.Services.Abstractions
{
    public interface IIdService<TEntity> where TEntity : IEntity
    {
        int GiveFreeId();
    }
}
