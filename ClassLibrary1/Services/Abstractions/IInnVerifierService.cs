﻿namespace Domain.Services.Abstractions
{
    public interface IInnVerifierService
    {
        bool IsValid(string inn);

        bool IsFree(string inn);
    }
}
