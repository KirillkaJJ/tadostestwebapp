﻿using System;
using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class ClientService : IClientService
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<Bill> _billRepository;
        private readonly IBillService _billService;
        private readonly IInnVerifierService _innVerifier;

        public ClientService(IRepository<Client> repository, IRepository<Bill> billRepository, 
            IBillService billService, IInnVerifierService innVerifier)
        {
            _clientRepository = repository;
            _billRepository = billRepository;
            _billService = billService;
            _innVerifier = innVerifier;
        }

        public void Add(string name, string innNum)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrEmpty(innNum))
                throw new ArgumentNullException(nameof(innNum));

            if (!_innVerifier.IsValid(innNum))
                throw new ArgumentException("Inn is not valid");

            if (!_innVerifier.IsFree(innNum))
                throw new ArgumentException("Client with the same Inn already created");

            _clientRepository.Add(new Client(name, innNum));
        }

        public void ChangeName(int id, string name)
        {
            if (id < 0)
                throw new ArgumentException("Id must be positive");

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Client client = _clientRepository.All().First(v => v.Id == id);

            if (client == null)
                throw new ArgumentException("Have no client with such ID");

            client.ChangeName(name);
        }

        public void Delete(int id)
        {
            if (id < 0)
                throw new ArgumentException("Id must be positive");

            _clientRepository.Delete(id);

            foreach (var clientsBillId in _billRepository.All()
                .Where(v => v.ClientId == id)
                .Select(v => v.Id))
            {
                _billService.Delete(clientsBillId);
            }

        }
    }
}