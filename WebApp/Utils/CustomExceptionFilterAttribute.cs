﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApp.Utils
{
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.Result =
                new JsonResult(new {Error = context.Exception.Message})
                {
                    StatusCode = (int) HttpStatusCode.InternalServerError
                };

            context.ExceptionHandled = true;
        }
    }
}